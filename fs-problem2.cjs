const fs = require("fs");
const path = require("path");

/*  Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the 
        new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into
         sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name 
        of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are
         mentioned in that list simultaneously.   */
function readWriteFile(filename) {
  fs.readFile(`${filename}`, "UTF-8", (error, data) => {
    if (error) {
      console.log(error);
    } else {
      console.log(data);
    }
  });

  fs.writeFile("filenames.txt", "", (error) => {
    if (error) {
      console.log(error);
    }
  });

  fs.readFile(`${filename}`, "UTF-8", (error, data) => {
    if (error) {
      console.log(error);
    }
    let fileName = "newFile1.txt";
    fs.writeFile(`${fileName}`, `${data}`.toUpperCase(), (error) => {
      if (error) {
        console.log(error);
      }

      fs.appendFile("filenames.txt", `${fileName} `, (error) => {
        if (error) {
          console.log(error);
        }
      });

      // read the new file and convert it to lower case
      fs.readFile(`${fileName}`, "UTF-8", (error, data) => {
        if (error) {
          console.log(error);
        } else {
          console.log(data);
        }
        let convert = data.toLowerCase().split(". ");
        let convertString = convert.join("\n");
        //   console.log(convertString);

        const fileName2 = "newFile2.txt";
        fs.writeFile(`${fileName2}`, `${convertString}`, (error) => {
          if (error) {
            console.log(error);
          }

          fs.appendFile("filenames.txt", `${fileName2} `, (error) => {
            if (error) {
              console.log(error);
            }
          });

          //   Read the new files, sort the content, write it out to a new file. Store the name
          //  of the new file in filenames.txt

          fs.readFile(`${fileName2}`, "UTF-8", (error, data) => {
            if (error) {
              console.log(error);
            } else {
              console.log(data);
            }

            let sortedcontent = convert.sort();

            //  console.log(sortedcontent);

            let sortedcontentString = sortedcontent.join("");

            //  console.log(sortedcontentString);

            let fileName3 = "newFile3.txt";
            fs.writeFile(`${fileName3}`, `${sortedcontentString}`, (error) => {
              if (error) {
                console.log(error);
              }

              fs.readFile(`${fileName3}`, "UTF-8", (error, data) => {
                if (error) {
                  console.log(error);
                } else {
                  console.log(data);
                }

                fs.appendFile("filenames.txt", `${fileName3}`, (error) => {
                  if (error) {
                    console.log(error);
                  }
                });

                // Read the contents of filenames.txt and delete all the new files that are
                // mentioned in that list simultaneously.

                fs.readFile("filenames.txt", "UTF-8", (error, data) => {
                  if (error) {
                    console.log(error);
                  } else {
                    //  console.log(data);
                  }

                  let fileInTxt = data.split(" ");

                  // console.log(fileInTxt)

                  fileInTxt.map((file) => {
                    if (file !== "") {
                      fs.unlink(`${file}`, (error) => {
                        if (error) {
                          console.log(error);
                        } else {
                          console.log("file is deleted successfully");
                        }
                      });
                    }
                  });
                });

                fs.unlink("filenames.txt", (error) => {
                  if (error) {
                    console.log(error);
                  }
                });
              });
            });
          });
        });
      });
    });
  });
}

module.exports = readWriteFile;
