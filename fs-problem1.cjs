const fs = require("fs");
const path = require("path");

function createFiles(directory, numberOfFiles) {
  fs.mkdir(directory, (error) => {
    if (error) {
      console.error(error);
    } else {
      console.log("Directory  created successfully.");

      if (numberOfFiles == 0) {
        deleteDirectory(directory);
      } else {
        createDifferentFiles(directory, numberOfFiles);
      }
    }
  })
}

function createDifferentFiles(directory, numberOfFiles) {
    let executeCount = 0;
    let errorCount = 0;
    let storedFileName = [];
    //    for creating multiple files
    for (let index = 0; index < numberOfFiles; index++) {
        const filename = `${index + 1}.json`;
        // store filenames in an array to delete those files later

        fs.writeFile(`${directory}/${filename}`, "", (error, data) => {
        if (error) {
            errorCount += 1;
        } else {
            executeCount += 1;
            storedFileName.push(filename);
      }

      // this is the condition when callback function called , after all files created
      if (executeCount + errorCount == numberOfFiles) {
        callback(error, storedFileName);
      }
    });
  }

  const callback = (err, data) => {
    if (err) {
      console.error(err);
    } else {
      // console.log(data);
      console.log(" all files created successfully");
      // call function for deleting files
      deleteFiles(data, directory);
    }
  };
}

function deleteFiles(storedFileName, directory) {

  const callback = (err, data) => {
    if (err) {
      console.error(err);
    } else {
      console.log(data);

      deleteDirectory(directory);
    }
  };

  let count = 0;
  for (let index = 0; index < storedFileName.length; index++) {
    fs.unlink(`${directory}/${storedFileName[index]}`, (error) => {
      count++;

      if (count == storedFileName.length) {
        callback(error, `${"All files deleted successfully"}`);
      }
    });
  }
}

function deleteDirectory(directory) {
  fs.rmdir(`${directory}`, (error) => {
    if (error) {
      console.log(error);
    } else {
      //  console.log(' Directory deleted')
    }
  });
}
module.exports = createFiles;


